const log = require('../../helpers/logger')(module);
const TokensStrorage = require('../../models/tokensStorage');

module.exports = async (options) => {
  try {
    const ts = { ...options };
    ts.dateCreate = new Date();
    await (new TokensStrorage(ts)).save();
    return ts;
  } catch (err) {
    log.error(err);
    throw new Error(err.message);
  }
};
