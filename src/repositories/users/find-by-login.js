const log = require('../../helpers/logger')(module);
const User = require('../../models/user');

module.exports = async (login) => {
  try {
    const user = await User.findOne({ email: login });
    return user;
  } catch (err) {
    log.error(err);
    throw err;
  }
};
