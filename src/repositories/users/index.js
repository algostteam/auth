const findByLogin = require('./find-by-login');
const checkPassword = require('./check-password');

module.exports = {
  findByLogin,
  checkPassword,
};
