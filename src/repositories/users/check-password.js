const log = require('../../helpers/logger')(module);

module.exports = async (user, password) => {
  try {
    user.checkPassword(password);
  } catch (err) {
    log.error(err);
    throw err;
  }
};

