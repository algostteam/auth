const mongoose = require('../helpers/mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  accessToken: {
    type: String,
    required: true,
  },
  refreshToken: {
    type: String,
    required: true,
  },
  dateCreate: {
    type: Date,
    required: true,
  },
}, { usePushEach: true });

module.exports = mongoose.model('TokensStorage', schema);
