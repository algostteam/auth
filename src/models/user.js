const crypto = require('crypto');
const mongoose = require('../helpers/mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    default: '',
  },
  DOB: {
    type: Date,
    default: new Date(0),
  },
  mainPhoto: {
    type: String,
    default: '',
  },
  education: {
    type: String,
    default: '',
  },
  hobbies: {
    type: String,
    default: '',
  },
  aboutSelf: {
    type: String,
    default: '',
  },
  salt: {
    type: String,
    required: true,
  },
  hashedPassword: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    default: 'user',
  },
  friends: {
    type: [
      {
        type: Schema.Types.ObjectId,
        require: true,
      },
    ],
    default: [],
  },
  communities: {
    type: [
      {
        type: Schema.Types.ObjectId,
        require: true,
      },
    ],
    default: [],
  },
  articles: {
    type: [
      {
        type: Schema.Types.ObjectId,
        require: true,
      },
    ],
    default: [],
  },
}, { usePushEach: true });

schema.virtual('password')
  .set(function set(password) {
    this.plainPassword = password;
    this.salt = crypto.randomBytes(32).toString('base64');
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function get() {
    return this.plainPassword;
  });

schema.methods.encryptPassword = function encryptPassword(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.methods.checkPassword = function checkPassword(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

module.exports = mongoose.model('User', schema);
