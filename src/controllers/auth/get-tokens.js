const crypto = require('crypto');

const log = require('../../helpers/logger')(module);
const config = require('../../config');
const ResponseJson = require('../../helpers/responseJson');
const userRepositoies = require('../../repositories/users');
const tokensStrorageRepositories = require('../../repositories/tokensStorages');
const descriptionConst = require('../constants/description');
const errorConst = require('../constants/errorCode');

module.exports = async (req, res, next) => {
  try {
    const rj = new ResponseJson();
    const { login, password } = req.body;
    const user = userRepositoies.findByLogin(login);
    let randomNumber;
    if (!user || !userRepositoies.checkPassword(user, password)) {
      rj.setAll(400, descriptionConst.WRONG_LOGIN_OR_PASSWORD, errorConst.WRONG_LOGIN_OR_PASSWORD);
      return res.status(rj.status).json();
    }
    // todo проверить нет ли таких токенов в базе
    randomNumber = crypto.randomBytes(32).toString('base64');
    const accessToken = crypto.createHmac('sha1', config.get('tokens:accessSecret')).update(randomNumber).digest('hex');
    randomNumber = crypto.randomBytes(32).toString('base64');
    const refreshToken = crypto.createHmac('sha1', config.get('tokens:accessSecret')).update(randomNumber).digest('hex');
    const tokens = await tokensStrorageRepositories({
      accessToken,
      refreshToken,
      // eslint-disable-next-line
      userId: user._id,
    });
    rj.set('tokens', tokens);
    rj.setDescription(descriptionConst.AUTH_SUCCESS);
    return res.status(rj.status).json(rj);
  } catch (err) {
    log.error(err);
    return next(500);
  }
};
